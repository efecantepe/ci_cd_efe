from dictionary import addValues


def test_addValues():
    result = addValues(5, 6)
    expectedResult = 11
    assert result == expectedResult
